DROP DATABASE IF EXISTS `go_trip_project`;

CREATE DATABASE `go_trip_project`;

USE `go_trip_project`;

-- Create the table role
CREATE TABLE IF NOT EXISTS role
(
    id    BIGINT(12) AUTO_INCREMENT,
    name VARCHAR(100) NOT NULL,
    CONSTRAINT role_uk UNIQUE (name),
    CONSTRAINT role_pk PRIMARY KEY (id)
);

-- Insert data into the role table
INSERT INTO role(name)
VALUES ('ROLE_USER'),
       ('ROLE_ADMIN');

-- Create the user table
CREATE TABLE IF NOT EXISTS `user`
(
    id             BIGINT PRIMARY KEY AUTO_INCREMENT,
    username       VARCHAR(100) UNIQUE NOT NULL,
    password       VARCHAR(100)        NOT NULL,
    avatar         VARCHAR(100),
    is_activated   bit(1) DEFAULT 0,
    is_deleted     bit(1) DEFAULT 0,
    remember_token VARCHAR(200),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

INSERT INTO `user` (username, password)
VALUES ('thanhthao', '$2a$10$rHI70FSKXINH5kgU/xpgyO1Sm8JHdJBo7Kn5J4oY/aqEWmCHGKm6e'),
		('minhphong', '$2a$10$rHI70FSKXINH5kgU/xpgyO1Sm8JHdJBo7Kn5J4oY/aqEWmCHGKm6e'),
		('hungtrong', '$2a$10$rHI70FSKXINH5kgU/xpgyO1Sm8JHdJBo7Kn5J4oY/aqEWmCHGKm6e');

-- password abcd1234

-- Create the oathu_user table
CREATE TABLE oauth_user (
    id BIGINT AUTO_INCREMENT,
    oauthId VARCHAR(255) NOT NULL,
    provider VARCHAR(50) NOT NULL,
    email VARCHAR(255) NOT NULL UNIQUE,
    name VARCHAR(255) NOT NULL,
    user_id BIGINT NOT NULL,
    CONSTRAINT ou_pk PRIMARY KEY(id),
    CONSTRAINT ou_user_fk FOREIGN KEY (user_id) REFERENCES user(id)
);

-- Create the table user_role
CREATE TABLE IF NOT EXISTS user_role
(
    id      BIGINT PRIMARY KEY AUTO_INCREMENT,
    user_id BIGINT NOT NULL,
    role_id BIGINT NOT NULL,
    CONSTRAINT user_role_user_fk FOREIGN KEY (user_id) REFERENCES `user` (id),
    CONSTRAINT user_role_role_fk FOREIGN KEY (role_id) REFERENCES `role` (id)
);
INSERT INTO user_role(user_id, role_id)
VALUES (1, 1),
       (1, 2);

-- Create the table category
CREATE TABLE IF NOT EXISTS category (
    id BIGINT AUTO_INCREMENT,
    name varchar(100) NOT NULL UNIQUE,
    CONSTRAINT category_pk PRIMARY KEY (id)
);

INSERT INTO category(name)
VALUES ('Nhà hàng và Quán ăn'),
       ('Giải trí và Vui chơi'),
       ('Mua sắm'),
       ('Du lịch và Tham quan'),
       ('Sức khỏe và Làm đẹp'),
       ('Giáo dục và Đào tạo'),
       ('Công nghệ và Kinh doanh');

-- Create the table place
CREATE TABLE IF NOT EXISTS place (
    id BIGINT AUTO_INCREMENT,
    name VARCHAR(100) NOT NULL,
    district VARCHAR(50) NOT NULL,
    province VARCHAR(100) NOT NULL,
    address VARCHAR(255) NOT NULL,
    average_rating FLOAT,
    latitude FLOAT NOT NULL,
    longitude FLOAT NOT NULL,
    views INT DEFAULT 0,
    arrivals INT DEFAULT 0,
    description TEXT,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    user_id BIGINT ,
    CONSTRAINT place_pk PRIMARY KEY (id),
    CONSTRAINT place_user_fk FOREIGN KEY (user_id) REFERENCES user(id)
);

INSERT INTO place(name,district, province, address, latitude, longitude,  average_rating, description)
VALUES ('Hồ Gươm','Hoàn Kiếm', 'Hà Nội', '1-8 P.Lê Thái Tổ, Hàng Trống', 21.0284793,105.8500046 , 4.5, 'Hồ Gươm là một trong những địa điểm nổi tiếng tại Hà Nội, Việt Nam.'),
       ('Chợ Bến Thành', 'Quận 1', 'Hồ Chí Minh', 'Đường Lê Lai, phường Bến Thành', 10.7724091, 106.695638, 4.6, 'Chợ Bến Thành là một trong những khu chợ lớn và sầm uất nhất của Thành phố Hồ Chí Minh, nổi tiếng với các sản phẩm thủ công mỹ nghệ và ẩm thực đặc trưng.');

-- Create the table place_category
CREATE TABLE IF NOT EXISTS place_category (
    id BIGINT AUTO_INCREMENT,
    place_id BIGINT,
    category_id BIGINT,
    CONSTRAINT pc_pk PRIMARY KEY (id),
    CONSTRAINT pl_place_fk FOREIGN KEY (place_id) REFERENCES place(id),
    CONSTRAINT pl_category_fk FOREIGN KEY (category_id) REFERENCES category(id)
);

INSERT INTO place_category(place_id, category_id)
VALUES (1, 4),
       (2, 3),
       (2, 4);


-- Create the table comment
CREATE TABLE IF NOT EXISTS comment (
    id BIGINT AUTO_INCREMENT,
    username VARCHAR(100),
	rating INT,
    content TEXT,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    place_id BIGINT,
    user_id BIGINT,
    CONSTRAINT comment_pk PRIMARY KEY (id),
    CONSTRAINT comment_place_fk FOREIGN KEY (place_id) REFERENCES place(id),
    CONSTRAINT comment_user_fk FOREIGN KEY (user_id) REFERENCES user(id)
);

INSERT INTO comment(username, content, rating, place_id, user_id)
VALUES ('thaonguyen', 'very good', 5, 1, 1);

-- Create the table image (
CREATE TABLE IF NOT EXISTS image (
    id BIGINT AUTO_INCREMENT,
    url TEXT,
    place_id BIGINT,
    comment_id BIGINT,
    CONSTRAINT img_pk PRIMARY KEY (id),
    CONSTRAINT img_place_fk FOREIGN KEY (place_id) REFERENCES place(id),
	CONSTRAINT img_comment_fk FOREIGN KEY (comment_id) REFERENCES comment(id)
);

INSERT INTO image( place_id, url)
VALUES (1, 'https://static.vinwonders.com/production/ho-hoan-kiem-2.jpg'),
        (2, 'https://cdn.tuoitre.vn/zoom/700_525/471584752817336320/2023/10/16/cho-ben-thanh-16974315197922025739761-208-0-1255-2000-crop-16974315958211438511062.jpg');

