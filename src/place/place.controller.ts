import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  Res,
  UploadedFiles,
  UseInterceptors,
} from '@nestjs/common';
import { PlaceService } from './place.service';
import { PlaceRequestDto } from './dto/place.request.dto';
import { PaginationQueryDto } from 'src/global/paginationQuery.dto';
import { FilesInterceptor } from '@nestjs/platform-express';

@Controller('/places')
export class PlaceController {
  constructor(private readonly placeService: PlaceService) {}

  @Get()
  getAllPlaces(@Query() paginationQuery: PaginationQueryDto, @Res() res) {
    return this.placeService.getPlaces(paginationQuery, res);
  }

  @Get('/:id')
  getPlaceById(@Param('id') id: string, @Res() res) {
    return this.placeService.getPlaceById(+id, res);
  }

  @Post()
  @UseInterceptors(FilesInterceptor('images'))
  createPlace(
    @UploadedFiles() images: Express.Multer.File[],
    @Body() body: PlaceRequestDto,
    @Res() res,
  ) {
    return this.placeService.savePlace(body, images, res);
  }

  @Put()
  updatePlace(@Body() body: PlaceRequestDto, @Res() res) {
    return this.placeService.savePlace(body, null, res);
  }

  @Delete('/:id')
  deletePlace(@Param('id') id: string, @Res() res) {
    return this.placeService.removePlace(+id, res);
  }
}
