import {
  IsNotEmpty,
  IsNumber,
  IsString,
  MaxLength,
  Min,
  Max,
  IsOptional,
} from 'class-validator';

export class PlaceRequestDto {
  @IsOptional()
  @IsNumber()
  id?: number;
  @IsNotEmpty()
  @IsString()
  @MaxLength(100)
  name: string;
  @IsNotEmpty()
  @IsString()
  @MaxLength(100)
  address: string;
  @IsNotEmpty()
  @IsString()
  @MaxLength(50)
  district: string;
  @IsNotEmpty()
  @IsString()
  @MaxLength(100)
  province: string;
  @IsNotEmpty()
  @IsNumber()
  @Min(-90)
  @Max(90)
  latitude: number;
  @IsNotEmpty()
  @IsNumber()
  @Min(-180)
  @Max(180)
  longitude: number;
  @IsString()
  description: string;
}
