import {
  BadRequestException,
  HttpStatus,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { PrismaClient } from '@prisma/client';
import { Response } from 'express';
import { PlaceRequestDto } from './dto/place.request.dto';
import { validate } from 'class-validator';
import { uploadToCloudinary } from 'src/utils/uploadToCloudinay';
import { ApiResponseDto } from 'src/global/api.response.dto';
import { PaginationQueryDto } from 'src/global/paginationQuery.dto';

@Injectable()
export class PlaceService {
  prisma = new PrismaClient();

  async getPlaces(paginationQuery: PaginationQueryDto, res: Response) {
    const skip = (paginationQuery?.page - 1) * paginationQuery?.size;
    const take = paginationQuery?.size;
    const sortBy = paginationQuery?.sortBy;
    const order = paginationQuery?.order;
    const orderBy =
      sortBy && order
        ? {
            [sortBy]: order,
          }
        : {};
    const data = await this.prisma.place.findMany({
      skip,
      take,
      orderBy,
      include: {
        images: {
          select: {
            url: true,
          },
        },
      },
    });
    const response = new ApiResponseDto<any>(
      HttpStatus.OK,
      data,
      'Succesfully',
      '',
    );
    return res.status(200).json({ response });
  }

  async getPlaceById(id: number, res: Response): Promise<any> {
    const data = await this.prisma.place.findFirst({
      where: {
        id,
      },
      include: {
        images: {
          select: {
            url: true,
          },
        },
      },
    });
    let response;
    if (data) {
      response = new ApiResponseDto<any>(
        HttpStatus.OK,
        data,
        'Succesfully',
        '',
      );
      return res.status(200).json({ response });
    } else {
      response = new ApiResponseDto<any>(
        HttpStatus.NOT_FOUND,
        '',
        'Place not found',
        '',
      );
      return res.status(404).json({ response });
    }
  }

  async savePlace(
    body: PlaceRequestDto,
    images: Express.Multer.File[],
    res: Response,
  ) {
    const placeDto = Object.assign(new PlaceRequestDto(), body);
    const errors = await validate(placeDto);
    if (errors.length > 0) {
      throw new BadRequestException(errors);
    }
    // save place to db
    // TODO: get current user and set user_id in place table
    const savedPlace = placeDto?.id
      ? await this.prisma.place.update({
          where: { id: placeDto.id },
          data: {
            ...body,
          },
        })
      : await this.prisma.place.create({
          data: {
            ...body,
          },
        });
    // upload image to cloudinay + save url to db
    try {
      let uploadResult;
      for (const file of images || []) {
        try {
          uploadResult = await uploadToCloudinary(file);
          await this.prisma.image.create({
            data: {
              place_id: savedPlace.id,
              url: uploadResult.secure_url,
            },
          });
        } catch (error) {
          console.error(
            `Failed to upload ${file.originalname} to Cloudinary:`,
            error,
          );
        }
      }

      const response = new ApiResponseDto<any>(
        HttpStatus.CREATED,
        savedPlace,
        'Successfully',
        '',
      );
      return res.status(200).json({ response });
    } catch (err) {
      const response = new ApiResponseDto<any>(
        HttpStatus.INTERNAL_SERVER_ERROR,
        '',
        'Fail',
        '',
      );
      return res.status(500).json({ response });
    }
  }

  async removePlace(id: number, res: Response) {
    const data = await this.prisma.place.findFirst({
      where: {
        id,
      },
    });
    if (!data) {
      throw new NotFoundException('Place not found');
    }

    const result = await this.prisma.place.delete({
      where: {
        id,
      },
    });

    if (result) {
      const response = new ApiResponseDto<any>(
        HttpStatus.OK,
        '',
        'Successfully',
        '',
      );
      return res.status(200).json({ response });
    } else {
      const response = new ApiResponseDto<any>(
        HttpStatus.INTERNAL_SERVER_ERROR,
        '',
        'Fail',
        '',
      );
      return res.status(500).json({ response });
    }
  }
}
