import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
      transformOptions: {
        enableImplicitConversion: true, // Allows for transformation of query/form data to DTO instance
      },
    }),
  );
  app.setGlobalPrefix('api');
  await app.listen(3000);
}
bootstrap();
