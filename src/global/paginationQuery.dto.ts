import { Transform, Type } from 'class-transformer';
import { IsInt, IsOptional, IsString, Min } from 'class-validator';

export class PaginationQueryDto {
  @IsOptional()
  @IsInt()
  @Min(1)
  @Type(() => Number)
  page?: number = 1;

  @IsOptional()
  @IsInt()
  @Min(1)
  @Type(() => Number)
  size?: number = 10;
  @IsOptional()
  @IsString()
  @Transform(({ value }) => value.toLowerCase())
  sortBy?: string;
  @IsOptional()
  @IsString()
  @Transform(({ value }) => value.toLowerCase())
  order: string;
}
