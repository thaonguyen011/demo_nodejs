import { HttpStatus } from '@nestjs/common';

export class ApiResponseDto<D> {
  data: D | D[];
  statusCode: HttpStatus;
  message: string;
  error: string;

  constructor(
    statusCode: number,
    data: D | D[],
    message: string,
    error: string,
  ) {
    this.data = data;
    this.statusCode = statusCode;
    this.message = message;
    this.error = error;
  }
}
