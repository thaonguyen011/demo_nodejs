import * as cloudinary from 'cloudinary';

export async function uploadToCloudinary(
  file: Express.Multer.File,
): Promise<any> {
  return new Promise((resolve, reject) => {
    const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1e9);
    const uniquePublicId = `${process.env.CLOUDINARY_FOLDER}/${file.originalname.split('.')[0]}-${uniqueSuffix}`;

    cloudinary.v2.uploader
      .upload_stream(
        {
          public_id: uniquePublicId,
        },
        (error, result) => {
          if (error) reject(error);
          else resolve(result);
        },
      )
      .end(file.buffer);
  });
}
